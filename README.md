# SWAT4HCLS 2024

# Workflow RO-Crate profile 2.0

> This README provides an overview of the poster created and presented at the [SWAT4HCLS 2024](https://www.swat4ls.org/) conference. The poster can be found in the `poster.pdf` file specified at the root.

> This README is highly inspired by [this page](https://about.workflowhub.eu/Workflow-RO-Crate/) which describes the *Workflow RO-Crate profile 1.0* and [this page](https://www.researchobject.org/ro-crate/1.1/workflows.html).

* Authors:
  * [George Marchment](https://orcid.org/0000-0002-4565-3940)
  * Marie Schmit 
* Title: Workflow RO-Crate profile 2.0
* Version: 2.0

_Workflow RO-Crates_ are a specialization of [_RO-Crate_](https://researchobject.github.io/ro-crate/) for packaging an executable workflow with all necessary documentation. It extends the more general [Bioschemas ComputationalWorkflow profile](https://bioschemas.org/profiles/ComputationalWorkflow/1.0-RELEASE).

## Table of Contents

- [SWAT4HCLS 2024](#swat4hcls-2024)
- [Workflow RO-Crate profile 2.0](#workflow-ro-crate-profile-20)
  - [Table of Contents](#table-of-contents)
  - [Concepts](#concepts)
    - [Context](#context)
    - [Metadata File Descriptor](#metadata-file-descriptor)
    - [Main Workflow File](#main-workflow-file)
    - [Main Workflow](#main-workflow)
    - [Subworkflow](#subworkflow)
    - [Process](#process)
    - [Describing inputs and outputs](#describing-inputs-and-outputs)
  - [Complete Workflow Example](#complete-workflow-example)

## Concepts

This section uses terminology from the [RO-Crate 1.1 specification](https://w3id.org/ro/crate/1.1).

### Context 

The _Crate_ JSON-LD MUST be valid according to [RO-Crate 1.1](https://w3id.org/ro/crate/1.1) and SHOULD use the RO-Crate 1.1 `@context` [https://w3id.org/ro/crate/1.1/context](https://w3id.org/ro/crate/1.1/context)

### Metadata File Descriptor

The [Metadata File Descriptor](https://www.researchobject.org/ro-crate/1.1/root-data-entity.html#ro-crate-metadata-file-descriptor) `conformsTo` SHOULD be an array that contains at least [https://w3id.org/ro/crate/1.1](https://w3id.org/ro/crate/1.1) and [https://w3id.org/workflowhub/workflow-ro-crate/1.0](https://w3id.org/workflowhub/workflow-ro-crate/1.0)

### Main Workflow File

The _Crate_ MUST contain a data entity of type `["File", "SoftwareSourceCode"]` as the _Main Workflow File_ entry point. This is the file which contains the main of the workflow.

The _Crate_ MUST refer to the _Main Workflow_ via `mainEntity`.

The _Main Workflow File_ MUST refers to all the elements which are defined in the file.

### Main Workflow 

The _Main Workflow File_ refers to the data entity *Main Workflow* of type `["SoftwareSourceCode", "ComputationalWorkflow"]`. This is the entity which describes the main of the workflow.

The _Main Workflow_ MUST refers to all the elements which are called in the main.

### Subworkflow 

The *Subworkflow* entity is essentially a specialised version of the *Main Workflow*.  The only difference is that instead of describing the main of the workflow, it describes the subworkflow.

The _Subworkflow_ MUST refers to all the elements which are called in the subworkflow.


### Process

The *Process* entity describes an individual step of a workflow. Depending on the workflow system used, it can be interchanged with *Module* or *Rule*. Its type is `["SoftwareSourceCode", "Script"]`.

The _Process_ MUST refers to all the tools which are used.


### Describing inputs and outputs

The input and output _parameters_ for a *Subworkflow* or *Process* can be given with `input` and `output` to [FormalParameter](https://bioschemas.org/types/FormalParameter/0.1-DRAFT-2020_07_21) contextual entities. Note that this entity usually represent a _potential_ input/output value in a reusable workflow, much like [function parameter definitions](https://en.wikipedia.org/wiki/Parameter_(computer_programming)) in general programming.

If complying with the Bioschemas [FormalParameter profile](https://bioschemas.org/profiles/FormalParameter/0.1-DRAFT-2020_07_21), the _contextual entities_ for [FormalParameter](https://bioschemas.org/types/FormalParameter/0.1-DRAFT-2020_07_21), referenced by `input` or `output`, MUST describe: [name](http://schema.org/name), [additionalType](http://schema.org/additionalType), [encodingFormat](http://schema.org/encodingFormat)

The Bioschemas [FormalParameter profile](https://bioschemas.org/profiles/FormalParameter/0.1-DRAFT-2020_07_21) explains the above and lists additional properties that can be used, including [description](http://schema.org/description), [valueRequired](http://schema.org/valueRequired), [defaultValue](http://schema.org/defaultValue) and [identifier](http://schema.org/identifier).

A contextual entity conforming to the [FormalParameter profile](https://bioschemas.org/profiles/FormalParameter/0.1-DRAFT-2020_07_21) SHOULD declare the versioned profile URI using [conformsTo](http://purl.org/dc/terms/conformsTo), e.g.:

```json
{
  "@id": "#36aadbd4-4a2d-4e33-83b4-0cbf6a6a8c5b",
  "@type": "FormalParameter",
  "conformsTo": 
    {"@id": "https://bioschemas.org/profiles/FormalParameter/0.1-DRAFT-2020_07_21/"},
  "..": ""
}
```

> `input`, `output` and `FormalParameter` are at time of writing proposed by Bioschemas and not yet integrated in Schema.org

## Complete Workflow Example

```json
{
  "@context": "https://w3id.org/ro/crate/1.1/context",
  "@graph": [
    {
      "@id": "ro-crate-metadata-exampleWF.json",
      "@type": "CreativeWork",
      "about": {
        "@id": "./"
      },
      "conformsTo": [
        {
          "@id": "https://w3id.org/ro/crate/1.1"
        }
      ]
    },
    {
      "@id": "./",
      "@type": "Dataset",
      "name": "exampleWF",
      "datePublished": "2024-02-19",
      "description": "Description of ExampleWF",
      "mainEntity": {
        "@id": "main.nf"
      },
      "license": {
        "@id": "mit"
      },
      "author": [
        {
          "@id": "George"
        }
      ],
      "maintainer": [
        {
          "@id": "George"
        }
      ],
      "hasPart": [
        {
          "@id": "main.nf"
        },
        {
          "@id": "sub.nf"
        },
        {
          "@id": "main.nf",
          "name": "main.nf",
          "@type": [
            "File",
            "SoftwareSourceCode"
          ],
          "programmingLanguage": {
            "@id": "https://w3id.org/workflowhub/workflow-ro-crate#nextflow"
          },
          "contentSize": 1.718,
          "dateCreated": "2024-02-19",
          "dateModified": "2024-02-19",
          "creator": [
            {
              "@id": "George"
            }
          ],
          "isPartOf": [],
          "hasPart": [
            {
              "@id": "sub.nf"
            },
            {
              "@id": "main.nf/MAIN"
            }
          ]
        },
        {
          "@id": "sub.nf",
          "name": "sub.nf",
          "@type": [
            "File",
            "SoftwareSourceCode"
          ],
          "programmingLanguage": {
            "@id": "https://w3id.org/workflowhub/workflow-ro-crate#nextflow"
          },
          "contentSize": 6.114,
          "dateCreated": "2024-02-19",
          "dateModified": "2024-02-19",
          "creator": [
            {
              "@id": "George"
            }
          ],
          "isPartOf": [
            {
              "@id": "main.nf"
            }
          ],
          "hasPart": [
            {
              "@id": "sub.nf/subworkflow"
            },
            {
              "@id": "sub.nf/process"
            }
          ]
        },
        {
          "@id": "sub.nf/subworkflow",
          "name": "Subworkflow",
          "@type": [
            "SoftwareSourceCode",
            "ComputationalWorkflow"
          ],
          "input": [],
          "output": [],
          "isPartOf": [
            {
              "@id": "sub.nf"
            }
          ],
          "hasPart": [
            {
              "@id": "sub.nf/process"
            }
          ]
        },
        {
          "@id": "sub.nf/process",
          "name": "Process",
          "@type": [
            "SoftwareSourceCode",
            "Script"
          ],
          "input": [
            {
              "@id": "in"
            }
          ],
          "output": [
            {
              "@id": "out"
            }
          ],
          "isPartOf": [
            {
              "@id": "sub.nf"
            },
            {
              "@id": "sub.nf/subworkflow"
            }
          ],
          "hasPart": [
            {
              "@id": "tool1"
            }
          ]
        },
        {
          "@id": "in",
          "@type": "FormalParameter"
        },
        {
          "@id": "out",
          "@type": "FormalParameter"
        },
        {
          "@id": "tool1",
          "name": "Tool",
          "url": "https://some-url.org"
        },
        {
          "@id": "main.nf/MAIN",
          "name": "Main Workflow",
          "@type": [
            "SoftwareSourceCode",
            "ComputationalWorkflow"
          ],
          "input": [],
          "output": [],
          "isPartOf": [
            {
              "@id": "main.nf"
            }
          ],
          "hasPart": [
            {
              "@id": "sub.nf/subworkflow"
            }
          ]
        }
      ]
    }
  ]
}
```